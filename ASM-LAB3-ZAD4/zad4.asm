.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
.DATA
		cout dd ?
		cin  dd ?

		tab1 dw 1,2,3,4,5,6,7,8,9,1,1,2,3,4,5,6,7,8,9,1,1,2,3,4,5,6,7,8,9,0
		tab2 dw 1,2,3,4,5,6,7,8,9,1,1,2,3,4,5,5,7,8,9,1,1,2,3,4,5,6,7,8,9,0
		tekst db "Liczba ktora sie nie zgadza: %i , indeks: %i",0
		rozmiar dd $ - tekst
		bufor dd 10 dup(?)
		tmp  dw ?
		lznakow dd 0
		liczba dd ?

.CODE
main proc
invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX
	CLD
	lol:
	mov ECX, 30
	mov ESI, OFFSET tab1
	mov EDI, OFFSET tab2

	repe cmpsw
	jne pomylka

	jmp endo
	pomylka:
	push ECX
	dec ESI
	dec ESI
	dec edi
	dec EDI
	mov ESI, EDI
	lodsw
	mov tmp, ax
	pop ECX
	sub ECX, 30
	NOT ECX
	mov liczba, ECX
	inc liczba
	invoke wsprintfA, OFFSET bufor, OFFSET tekst, tmp,liczba
	invoke WriteConsoleA, cout,OFFSET bufor,rozmiar, OFFSET lznakow,0

	endo:



		invoke ExitProcess, 0


main endp

END